<?php

$option_name = 'apmmust_shipping_calculator';

add_action('wp_enqueue_scripts', function () {
  wp_enqueue_style('shipping-fee-calculator', plugin_dir_url(__FILE__) . 'assets/shipping-fee-calculator.css', [], mt_rand(1, 50000), 'all');
  wp_enqueue_script('shipping-fee-calculator', plugin_dir_url(__FILE__) . 'assets/shipping-fee-calculator.js', ['jquery'], mt_rand(1, 50000), true);
  wp_localize_script('shipping-fee-calculator', 'apmmust_ajax_obj', array('ajaxurl' => admin_url('admin-ajax.php')));
});

//--------- Meta Box - Start ----------------//
// country
// shipping_method
// shipping-zone
// box_dimension
// box_dimension_group
// width
// height
// depth
// kg_fomula
// ems
// EMS
// ups
// UPS
// fedex
// FedEx
// dhl
// DHL
// estimate_date
// estimate_shipping_date
// estimate_shipping_date_group
// shipping_fee_kg
// price_per_weight

// metabox를 어드민 메뉴에서 숨긴다
// add_action('admin_init', 'apmmust_remove_menu_metabox');
// function apmmust_remove_menu_metabox()
// {
//   remove_menu_page('meta-box');
// }

// Custom Post Type 설정
add_action('init', 'apmmust_resiter_shipping_zone_post_type');
function apmmust_resiter_shipping_zone_post_type()
{
  $args = [
    'label' => esc_html__('Shipping Zones', 'text-domain'),
    'labels' => [
      'menu_name' => esc_html__('Shipping Zones', 'apmmust'),
      'name_admin_bar' => esc_html__('Shipping Zone', 'apmmust'),
      'add_new' => esc_html__('Add Shipping Zone', 'apmmust'),
      'add_new_item' => esc_html__('Add new Shipping Zone', 'apmmust'),
      'new_item' => esc_html__('New Shipping Zone', 'apmmust'),
      'edit_item' => esc_html__('Edit Shipping Zone', 'apmmust'),
      'view_item' => esc_html__('View Shipping Zone', 'apmmust'),
      'update_item' => esc_html__('View Shipping Zone', 'apmmust'),
      'all_items' => esc_html__('All Shippingg Zones', 'apmmust'),
      'search_items' => esc_html__('Search Shipping Zones', 'apmmust'),
      'parent_item_colon' => esc_html__('Parent Shipping Zone', 'apmmust'),
      'not_found' => esc_html__('No Shipping Zones found', 'apmmust'),
      'not_found_in_trash' => esc_html__('No Shipping Zones found in Trash', 'apmmust'),
      'name' => esc_html__('Shipping Zones', 'apmmust'),
      'singular_name' => esc_html__('Shipping Zone', 'apmmust'),
    ],
    'public' => true,
    'exclude_from_search' => true,
    'publicly_queryable' => false,
    'show_ui' => true,
    'show_in_nav_menus' => false,
    'show_in_admin_bar' => true,
    'show_in_rest' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'has_archive' => true,
    'query_var' => false,
    'can_export' => false,
    'rewrite_no_front' => false,
    'show_in_menu' => true,
    'menu_icon' => 'dashicons-cart',
    'supports' => [
      'title',
    ],

    'rewrite' => true
  ];

  register_post_type('shipping-zone', $args);
}

// 박스 크기 설정 / 부피중량 계산법 / 국가별 배송 기간 설정 페이지 추가
add_filter('mb_settings_pages', function ($settings_pages) {
  global $option_name;

  $settings_pages[] = [
    'id' => $option_name,
    'option_name' => $option_name,
    'menu_title' => '기본 설정',
    'parent' => 'edit.php?post_type=shipping-zone',
  ];
  return $settings_pages;
});

// 박스 크기 설정 / 부피중량 계산법 / 국가별 배송 기간 필드 추가
add_filter('rwmb_meta_boxes', function ($meta_boxes) {
  global $option_name;

  // 박스 크기 설정
  $meta_boxes[] = [
    'title' => esc_html__('박스 크기 설정(CM)', 'apmmust'),
    'settings_pages' => $option_name,
    'fields' => [
      [
        'id' => 'box_dimension_group',
        'clone' => true,
        'type' => 'group',
        'fields' => [
          [
            'name' => esc_html__('가로', 'apmmust'),
            'id' => 'width',
            'type' => 'number',
            'columns' => 3,
          ],
          [
            'name' => esc_html__('세로', 'apmmust'),
            'id' => 'height',
            'type' => 'number',
            'columns' => 3,
          ],
          [
            'name' => esc_html__('높이', 'apmmust'),
            'id' => 'depth',
            'type' => 'number',
            'columns' => 3,
          ],
        ]
      ],
    ],
  ];

  // 부피중량 계산법
  $meta_boxes[] = [
    'id' => 'kg_fomula',
    'title' => esc_html__('부피중량 계산에 사용되는 값', 'apmmust'),
    'description' => '부피중량(kg) = 가로(cm) X 세로(cm) X 높이(cm) / 배송사별 값(아래 지정하는 값)',
    'settings_pages' => $option_name,
    'fields' => [
      [
        'name' => esc_html__('EMS', 'apmmust'),
        'id' => 'ems',
        'type' => 'number',
        'columns' => 3,
      ],
      [
        'name' => esc_html__('UPS', 'apmmust'),
        'id' => 'ups',
        'type' => 'number',
        'columns' => 3,
      ],
      [
        'name' => esc_html__('FedEx', 'apmmust'),
        'id' => 'fedex',
        'type' => 'number',
        'columns' => 3,
      ],
      [
        'name' => esc_html__('DHL', 'apmmust'),
        'id' => 'dhl',
        'type' => 'number',
        'columns' => 3,
      ],
    ],
  ];

  $countries_obj = new WC_Countries();
  $countries = $countries_obj->__get('countries');

  $meta_boxes[] = [
    'id' => 'estimate_shipping_date',
    'title' => esc_html__('배송사별 목적 국가 까지 배송되는데 소요되는 기간', 'apmmust'),
    'settings_pages' => $option_name,
    'fields' => [
      [
        'id' => 'estimate_shipping_date_group',
        'clone' => true,
        'type' => 'group',
        'fields' => [
          [
            'name' => esc_html__('국가', 'apmmust'),
            'id' => 'country',
            'type' => 'select_advanced',
            'placeholder' => '국가를 선택하세요',
            'options' => $countries,
            'columns' => 2,
          ],
          [
            'name' => esc_html__('EMS', 'apmmust'),
            'id' => 'ems',
            'type' => 'number',
            'std' => 0,
            'columns' => 2,
          ],
          [
            'name' => esc_html__('UPS', 'apmmust'),
            'id' => 'ups',
            'type' => 'number',
            'std' => 0,
            'columns' => 2,
          ],
          [
            'name' => esc_html__('FedEx', 'apmmust'),
            'id' => 'fedex',
            'type' => 'number',
            'std' => 0,
            'columns' => 2,
          ],
          [
            'name' => esc_html__('DHL', 'apmmust'),
            'id' => 'dhl',
            'type' => 'number',
            'std' => 0,
            'columns' => 2,
          ]
        ]
      ],
    ],
  ];

  return $meta_boxes;
});

// 국가 및 배송사별 배송비 설정
add_filter('rwmb_meta_boxes', function ($meta_boxes) {
  // Get WooCommerce countries
  $countries_obj = new WC_Countries();
  $countries = $countries_obj->__get('countries');

  $shipping_companies = [
    'ems' => [
      'label' => 'EMS',
      'icon' => 'dashicons-truck',
    ],
    'ups' => [
      'label' => 'UPS',
      'icon' => 'dashicons-truck',
    ],
    'fedex' => [
      'label' => 'FedEx',
      'icon' => 'dashicons-truck',
    ],
    'dhl' => [
      'label' => 'DHL',
      'icon' => 'dashicons-truck',
    ],
  ];

  $prices_per_weight_group_by_shipping_companies = array_map(function ($company) {
    $fields = [];
    for ($i = 1; $i <= 30; $i += 1) {
      $fields[] = [
        'name' => $i . 'kg',
        'id' => $i . 'kg',
        'type' => 'number',
        'columns' => 2,
      ];
    }

    return [
      'id' => 'price_per_weight_' . $company,
      'type' => 'group',
      'clone' => false,
      'sort_clone' => false,

      // List of sub-fields.
      'fields' => $fields,
      'tab' => $company,
    ];
  }, array_keys($shipping_companies));

  // Define the metabox
  $meta_boxes[] = [
    'id' => 'shipping_zone_details',
    'title' => esc_html__('Shipping Zone Details', 'apmmust'),
    'post_types' => 'shipping-zone',
    'context' => 'normal',
    'priority' => 'high',
    'tabs' => array_merge(
      [
        'country' => [
          'label' => '공통 국가',
          'icon' => 'dashicons-location',
          // Dashicon
        ],
      ],
      $shipping_companies
    ),
    'tab_style' => 'default',
    'fields' => array_merge(
      [
        [
          'name' => '국가 선택',
          'id' => 'country',
          'type' => 'select_advanced',
          'options' => $countries,
          'placeholder' => 'Select a Country',
          'tab' => 'country',
          'multiple' => true,
        ],
        ...$prices_per_weight_group_by_shipping_companies
      ],
    ),
  ];

  return $meta_boxes;
});

// 가격 조정 배율 필드
add_filter('rwmb_meta_boxes', function ($meta_boxes) {
  global $option_name;

  // 부피중량 계산법
  $meta_boxes[] = [
    'title' => esc_html__('배송사별 가격 배율 지정', 'apmmust'),
    'settings_pages' => $option_name,
    'fields' => [
      [
        'name' => esc_html__('EMS', 'apmmust'),
        'id' => 'price_scaling_by_shipping_company_ems',
        'type' => 'number',
        'min' => 0,
        'step' => 0.001,
        'columns' => 3,
      ],
      [
        'name' => esc_html__('UPS', 'apmmust'),
        'id' => 'price_scaling_by_shipping_company_ups',
        'type' => 'number',
        'min' => 0,
        'step' => 0.001,
        'columns' => 3,
      ],
      [
        'name' => esc_html__('FedEx', 'apmmust'),
        'id' => 'price_scaling_by_shipping_company_fedex',
        'type' => 'number',
        'min' => 0,
        'step' => 0.001,
        'columns' => 3,
      ],
      [
        'name' => esc_html__('DHL', 'apmmust'),
        'id' => 'price_scaling_by_shipping_company_dhl',
        'type' => 'number',
        'min' => 0,
        'step' => 0.001,
        'columns' => 3,
      ],
    ],
  ];

  return $meta_boxes;
});

//--------- Meta Box - End ----------------//


//--------- Util - Start ----------------//
function apmmust_get_weight_fomula()
{
  global $option_name;
  return array(
    'ems' => rwmb_meta('ems', ['object_type' => 'setting'], $option_name) ?: 6000,
    'ups' => rwmb_meta('ups', ['object_type' => 'setting'], $option_name) ?: 6000,
    'fedex' => rwmb_meta('fedex', ['object_type' => 'setting'], $option_name) ?: 6000,
    'dhl' => rwmb_meta('dhl', ['object_type' => 'setting'], $option_name) ?: 6000,
  );
}

// 공식에 따라서 demension에 무게를 추가해서 보내준다
function apmmust_get_shipping_box_dimensions()
{
  global $option_name;

  $fomula = apmmust_get_weight_fomula();
  $dimensions = rwmb_meta('box_dimension_group', ['object_type' => 'setting'], $option_name);

  if (!$dimensions) {
    return [];
  }

  $dimensions = array_map(function ($row) use ($fomula) {
    $multiple_result = intval($row['width']) * intval($row['height']) * intval($row['depth']);
    $row['weight'] = array(
      'ems' => ceil($multiple_result / $fomula['ems']),
      'ups' => ceil($multiple_result / $fomula['ups']),
      'fedex' => ceil($multiple_result / $fomula['fedex']),
      'dhl' => ceil($multiple_result / $fomula['dhl']),
    );
    return $row;
  }, $dimensions);

  return $dimensions;
}
function apmmust_find_row_in_shipping_zone($country, $shipping_company)
{
  global $option_name;

  // 먼저 예상 일을 찾아
  $estimate_shipping_date_group = rwmb_get_value('estimate_shipping_date_group', ['object_type' => 'setting'], $option_name);

  $estimate_shipping_date = null;
  foreach ($estimate_shipping_date_group as $row) {
    if ($row['country'] === $country) {
      $estimate_shipping_date = $row[$shipping_company];
      break;
    }
  }

  if ($estimate_shipping_date === null) {
    return null;
  }

  // 이제는 해당 국가의, company에 해당되는 그거를 찾자
  // 모든 ZONE을 다 돌아야함
  $args = [
    'post_type' => 'shipping-zone',
    'posts_per_page' => -1,
    'post_status' => 'publish'
  ];
  $shipping_zones = get_posts($args);

  // shipping_zones를 루프 톨면서 country를 가져오고,
  // country가 있다면 company 그룹 하위의 모든 무게별 배송비를 가져온다
  foreach ($shipping_zones as $shipping_zone) {
    $countries = rwmb_get_value('country', '', $shipping_zone->ID);
    if (in_array($country, $countries)) {
      $price_per_weight = rwmb_get_value('price_per_weight_' . $shipping_company, '', $shipping_zone->ID);

      if (!$price_per_weight) {
        return null;
      }

      // 1kg부터 30kg까지 모두 다 있어야해
      $allValuesExistAndNonZero = true;
      for ($i = 1; $i <= 30; $i++) {
        $key = $i . 'kg';
        if (!array_key_exists($key, $price_per_weight) || intval($price_per_weight[$key]) == 0) {
          $allValuesExistAndNonZero = false;
          break;
        }
      }

      if (!$allValuesExistAndNonZero) {
        return null;
      }

      return [
        'country' => $country,
        'shipping_company' => $shipping_company,
        'price_per_weight' => $price_per_weight,
        'estimate_shipping_date' => $estimate_shipping_date
      ];
    }
  }

  return null;
}

// shippint-zone post type에서 모든 country를 가져오고 중복을 제거한다
function apmmust_get_all_countries_for_shipping_calculator()
{
  $args = [
    'post_type' => 'shipping-zone',
    'posts_per_page' => -1,
    'post_status' => 'publish'
  ];
  $shipping_zones = get_posts($args);

  $country_codes = [];
  foreach ($shipping_zones as $shipping_zone) {
    $country_codes = array_merge($country_codes, rwmb_get_value('country', '', $shipping_zone->ID));
  }

  $country_codes = array_unique($country_codes);

  $woo_countries = WC()->countries->get_countries();

  $countries = array_map(function ($country_code) use ($woo_countries) {
    return [
      'code' => $country_code,
      'name' => $woo_countries[$country_code]
    ];
  }, $country_codes);

  // 정렬한다
  usort($countries, function ($a, $b) {
    return strcmp($a['name'], $b['name']);
  });


  return $countries;
}

function apmmust_get_shipping_company_by_country($country)
{
  $args = [
    'post_type' => 'shipping-zone',
    'posts_per_page' => -1,
    'post_status' => 'publish'
  ];
  $shipping_zones = get_posts($args);

  $shipping_companies = ['ems', 'ups', 'fedex', 'dhl'];

  $possible_shipping_companies = [];

  // shipping_zones를 루프 톨면서 country를 가져오고,
  // country가 있다면 company 그룹 하위의 모든 무게별 배송비를 가져온다
  foreach ($shipping_zones as $shipping_zone) {
    $countries = rwmb_get_value('country', '', $shipping_zone->ID);
    if (in_array($country, $countries)) {
      foreach ($shipping_companies as $shipping_company) {
        $price_per_weight = rwmb_get_value('price_per_weight_' . $shipping_company, '', $shipping_zone->ID);
        if (!$price_per_weight) {
          continue;
        }
        // 1kg부터 30kg까지 모두 다 있어야해
        $allValuesExistAndNonZero = true;
        for ($i = 1; $i <= 30; $i++) {
          $key = $i . 'kg';
          if (!array_key_exists($key, $price_per_weight) || intval($price_per_weight[$key]) == 0) {
            $allValuesExistAndNonZero = false;
            break;
          }
        }
        if (!$allValuesExistAndNonZero) {
          continue;
        }

        // 다 있다면 ?
        $possible_shipping_companies[] = $shipping_company;
      }
    }
  }

  return $possible_shipping_companies;
}

//--------- Util - End ----------------//


//--------- AJAX - Start ----------------//
// 국가와 배송 방법을 받아서 테이블을 돌려준다
add_action('wp_ajax_apmmust_calculate_shipping_fee_action', 'apmmust_calculate_shipping_fee_action');
add_action('wp_ajax_nopriv_apmmust_calculate_shipping_fee_action', 'apmmust_calculate_shipping_fee_action');
function apmmust_calculate_shipping_fee_action()
{
  global $option_name;

  if (!isset($_POST['country']) || !isset($_POST['shipping_company']) || !isset($_POST['weight'])) {
    wp_send_json_error(
      array(
        "code" => 401,
        "message" => 'no value'
      ),
      401
    );
    return;
  }

  $country = $_POST['country'];
  $shipping_company = $_POST['shipping_company'];
  $weight = $_POST['weight'];

  if (empty($country) || empty($shipping_company) || empty($weight)) {
    wp_send_json_error(
      array(
        "code" => 403,
        "message" => 'empty value'
      )
    );
    return;
  }

  $shipping_company = strtolower($shipping_company);
  if ($shipping_company !== 'ems' && $shipping_company !== 'ups' && $shipping_company !== 'fedex' && $shipping_company !== 'dhl') {
    wp_send_json_error(
      array(
        "code" => 402,
        "message" => 'invalid value'
      )
    );
    return;
  }

  $weight = intval($weight);

  if ($weight <= 0) {
    wp_send_json_error(
      array(
        "code" => 402,
        "message" => 'invalid value'
      )
    );
    return;
  }

  $row = apmmust_find_row_in_shipping_zone($country, $shipping_company);
  
  if ($row === null) {
    wp_send_json_error(
      array(
        "code" => 404,
        "message" => 'The Shipping Method is not supported for the country'
      )
    );
    return;
  }

  $divisor = 30;
  $quotient = intdiv($weight, $divisor);
  $remainder = $weight % $divisor;

  // 우선 30kg을 초과했다면 이래나 저래나 30kg중량으로 계산한다
  $price = $quotient * intval($row['price_per_weight']["30kg"] ?? 0);

  if ($remainder > 0) {
    // 30으로 나눈 나머지와 부피중량중 더 큰값을 사용한다
    $box_dimensions = apmmust_get_shipping_box_dimensions();

    $max_weight = array_reduce(array_reverse($box_dimensions), function ($acc, $cur) use ($shipping_company, $remainder) {
      $w = intval($cur['weight'][$shipping_company]);
      if ($remainder <= $w) {
        return $w;
      }
      return $acc;
    }, $divisor);

    $price += intval($row['price_per_weight']["{$max_weight}kg"]);
  }

  $krw_rate = 1300;
  if (class_exists('WOOMULTI_CURRENCY_Data')) {
    $data = WOOMULTI_CURRENCY_Data::get_ins();
    $krw_rate = ($data->get_exchange('USD', 'KRW')['KRW'] ?? 1300) ?: 1300;
  }

  $paypal_fee = (float) ((rwmb_meta('price_scaling_by_shipping_company_' . $shipping_company, ['object_type' => 'setting'], $option_name) ?? 1.043) ?: 1.043);
  $usd = number_format(($price / $krw_rate) * $paypal_fee, 2); // paypal 수수료 적용

  $result = array(
    'price_krw' => $price,
    'weight' => $weight,
    'price_usd' => $usd,
    'estimate_date' => intval($row['estimate_shipping_date']),
    'rate_krw' => $krw_rate,
  );

  wp_send_json_success($result);
  return;
}

add_action('wp_ajax_apmmust_shipping_fee_table_action', 'apmmust_shipping_fee_table_action');
add_action('wp_ajax_nopriv_apmmust_shipping_fee_table_action', 'apmmust_shipping_fee_table_action');
function apmmust_shipping_fee_table_action()
{
  global $option_name;

  if (!isset($_POST['country']) || !isset($_POST['shipping_company'])) {
    wp_send_json_error(
      array(
        "code" => 404,
        "message" => 'no value'
      )
    );
    return;
  }

  $country = $_POST['country'];
  $shipping_company = $_POST['shipping_company'];

  if (empty($country) || empty($shipping_company)) {
    wp_send_json_error(
      array(
        "code" => 403,
        "message" => 'empty country or shipping_company'
      ),
      401
    );
    return;
  }
  $shipping_company = strtolower($shipping_company);
  if ($shipping_company !== 'ems' && $shipping_company !== 'ups' && $shipping_company !== 'fedex' && $shipping_company !== 'dhl') {
    wp_send_json_error(
      array(
        "code" => 402,
        "message" => 'invalid value'
      )
    );
    return;
  }

  $row = apmmust_find_row_in_shipping_zone($country, $shipping_company);

  if ($row === null) {
    wp_send_json_error(
      array(
        "code" => 404,
        "message" => 'The Shipping Method is not supported for the country'
      )
    );
    return;
  }

  $krw_rate = 1300;
  if (class_exists('WOOMULTI_CURRENCY_Data')) {
    $data = WOOMULTI_CURRENCY_Data::get_ins();
    $krw_rate = $data->get_exchange('USD', 'KRW')['KRW'] ?? 1300;
  }
  $paypal_fee = (float) ((rwmb_meta('price_scaling_by_shipping_company_' . $shipping_company, ['object_type' => 'setting'], $option_name) ?? 1.043) ?: 1.043);

  $shipping_fee_table = [];
  foreach ($row['price_per_weight'] as $key => $price) {
    $usd = number_format(($price / $krw_rate) * $paypal_fee, 2); // paypal 수수료 적용
    $shipping_fee_table[] = $usd;
  }

  $result = array(
    'estimate_date' => intval($row['estimate_shipping_date']),
    'rate_krw' => $krw_rate,
    'table' => $shipping_fee_table
  );

  wp_send_json_success($result);
  return;
}

add_action('wp_ajax_apmmust_get_shipping_company_by_country_action', 'apmmust_get_shipping_company_by_country_action');
add_action('wp_ajax_nopriv_apmmust_get_shipping_company_by_country_action', 'apmmust_get_shipping_company_by_country_action');
function apmmust_get_shipping_company_by_country_action()
{
  if (!isset($_POST['country'])) {
    wp_send_json_error(
      array(
        "code" => 404,
        "message" => 'no country'
      )
    );
    return;
  }

  $country = $_POST['country'];
  $shipping_company = apmmust_get_shipping_company_by_country($country);
  $result = array(
    'shipping_company' => $shipping_company
  );

  wp_send_json_success($result);
}
//--------- AJAX - End ----------------//

!is_admin() && add_shortcode('apmmust_shipping_calculator', function ($attrs) {
  $countries_with_code = apmmust_get_all_countries_for_shipping_calculator();

  if (empty($countries_with_code)) { ?>
    <div class="woocommerce-NoticeGroup">
      <ul class="woocommerce-error" role="alert">
        <li>배송비가 설정되지 않았습니다. 관리자에게 문의해주세요</li>
      </ul>
    </div>
    <?php
  } else {

    $box_dimensions = apmmust_get_shipping_box_dimensions();
    ?>

    <div class="shipping-calculator-container">
      <h3>
        <?php echo __("Calculate your estimated shipping fee", "apmmust"); ?>
      </h3>
      <p id="shipping-calculator-error-message"></p>
      <ul>
        <li class="field country-field">
          <div class="left">
            <label>
              <?php echo __("Country", "apmmust") ?>
            </label>
          </div>
          <div class="right">
            <p>
              <select name="country">
                <option disabled selected>Select Country</option>
                <?php
                foreach ($countries_with_code as $country_with_code) { ?>
                  <option value="<?php echo $country_with_code['code']; ?>"><?php echo $country_with_code['name']; ?></option>
                  <?php
                } ?>
              </select>
            </p>
          </div>
        </li>
        <li class="field shipping-type">
          <div class="left">
            <label>
              <?php echo __("Shipping method", "apmmust") ?>
            </label>
          </div>
          <div class="right">
            <p>
              <select name="shipping_company" id="shipping_company">
                <option disabled selected>Select</option>
              </select>
            </p>
          </div>
        </li>
        <li class="field weight-field">
          <div class="left">
            <label>
              <?php echo __("Weight", "apmmust") ?> <span>(kg)</span>
            </label>
          </div>
          <div class="right">
            <p>
              <input type="number" name="weight" placeholder="0" />
            </p>
          </div>
        </li>
        <li class="field box-dimension-field">
          <div class="left">
            <label>Box dimension</label>
          </div>
          <div class="right">
            <p>
              <span class="box-dimension-text"></span>
              <select name="box-dimension" disabled hidden>
                <?php
                foreach ($box_dimensions as $dimension) {
                  $val = $dimension['width'] . '-' . $dimension['height'] . '-' . $dimension['depth'];
                  $full_name = $dimension['width'] . 'cm x ' . $dimension['height'] . 'cm x ' . $dimension['depth'] . 'cm';
                  ?>
                  <option value="<?php echo $val; ?>" weight-ems="<?php echo $dimension['weight']['ems']; ?>"
                    weight-ups="<?php echo $dimension['weight']['ups']; ?>"
                    weight-fedex="<?php echo $dimension['weight']['fedex']; ?>"
                    weight-dhl="<?php echo $dimension['weight']['dhl']; ?>"> <?php echo $full_name; ?>
                  </option>
                  <?php
                } ?>
              </select>
            </p>
          </div>
        </li>
      </ul>
      <div class="calculate-result invisible">
        <div class="estimate-price">
          <span>
            <?php echo __("Total price", "apmmust"); ?> :
          </span>
          <span></span>
        </div>
        <div class="estimate-date">
          <span>
            <?php echo __("Estimate date", "apmmust"); ?> :
          </span>
          <span></span>
        </div>
      </div>
      <div class="calculate-container">
        <button class="calculate-button" type="button">
          <?php echo __("Calculate", "apmmust") ?>
        </button>
      </div>
    </div>

    <div class="shipping-fee-table-container">
      <h3>
        <?php echo __("Shipping Price Table", "apmmust"); ?> ($)
      </h3>
      <p id="shipping-fee-table-error-message"></p>
      <ul>
        <li class="field">
          <div class="left">
            <label>
              <?php echo __("Country", "apmmust") ?>
            </label>
          </div>
          <div class="right">
            <p>
              <select name="country">
                <option disabled selected>Select Country</option>
                <?php
                foreach ($countries_with_code as $country_with_code) { ?>
                  <option value="<?php echo $country_with_code['code']; ?>"><?php echo $country_with_code['name']; ?></option>
                  <?php
                } ?>
              </select>
            </p>
          </div>
        </li>
        <li class="field">
          <div class="left">
            <label>
              <?php echo __("Shipping method", "apmmust") ?>
            </label>
          </div>
          <div class="right">
            <p>
              <select name="shipping_company_in_price_table" id="shipping_company_in_price_table">
                <option disabled selected>Select</option>
              </select>
            </p>
          </div>
        </li>
      </ul>
      <table class="price-table invisible">
        <?php
        for ($i = 1; $i <= 10; $i += 1) { ?>
          <tr>
            <?php
            for ($j = 0; $j < 3; $j += 1) {
              $index = $i + 10 * $j - 1;
              $kg = $index + 1; ?>
              <td id="<?php echo "kg-{$kg}"; ?>" class="kg"><?php echo $kg; ?>kg</td>
              <td id="<?php echo "price-kg-{$kg}" ?>">...</td>
              <?php
            } ?>
          </tr>
          <?php
        } ?>
      </table>
    </div>
    <?php
  }
});
