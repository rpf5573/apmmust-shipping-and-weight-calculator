<?php

require_once plugin_dir_path(__FILE__) . 'SelectNavWalker.php';

add_action('wp_enqueue_scripts', function () {
  wp_enqueue_style('weight-calculator', plugin_dir_url(__FILE__) . 'assets/weight-calculator.css', [], mt_rand(1, 50000), 'all');
  wp_enqueue_script('weight-calculator', plugin_dir_url(__FILE__) . 'assets/weight-calculator.js', ['jquery'], mt_rand(1, 50000), true);
});

// 무게 계산기를 만든다
add_filter('rwmb_meta_boxes', 'apmmust_add_weight_field_to_product_category');
function apmmust_add_weight_field_to_product_category($meta_boxes)
{
  $meta_boxes[] = array(
    'id' => 'product-category-weight-field',
    'title' => __('무게 계산기용 입력 필드', 'textdomain'),
    'taxonomies' => 'product_cat',
    'fields' => array(
      array(
        'id' => 'weight',
        'name' => __('무게(g)', 'textdomain'),
        'type' => 'number',
      ),
    ),
  );
  return $meta_boxes;
}

// shipping calculator를 위한 menu location 등록
add_action('init', function () {
  register_nav_menus(
    array(
      'weight_calculator_clothes' => '무게 계산용 의류 메뉴',
    )
  );
  register_nav_menus(
    array(
      'weight_calculator_accessories' => '무게 계산용 악세서리 메뉴',
    )
  );
});

!is_admin() && add_shortcode('apmmust_weight_calculator', function ($atts) {

  // Extract shortcode attributes
  extract(
    shortcode_atts(
      array(
        'taxonomy' => 'product_cat',
        'name' => 'product_cat_dropdown',
        'id' => 'product-cat-dropdown',
        'selected' => 0,
        'hierarchical' => 1,
        'echo' => 0,
        'hide_empty' => 1,
      ),
      $atts
    )
  );

  ob_start(); ?>

  <div class="product-category-weight-table-container">
    <h3>
      <?php echo __("Estimate the weight of your order (please note this is not an exact value, but merely a reference).", "apmmust"); ?>
    </h3>
    <div>
      <table id="clothes-dynamic-table">
        <thead>
          <tr>
            <th>Clothes</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <select id="clothes-dropdown">
                <option hidden value="" disabled selected>Select Clothes</option>
                <?php
                echo wp_nav_menu(
                  array(
                    'theme_location' => 'weight_calculator_clothes',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'walker' => new Select_Nav_Walker()
                  )
                );
                ?>
              </select>
            </td>
            <td>-</td>
          </tr>
        </tbody>
      </table>
      <table id="accessory-dynamic-table">
        <thead>
          <tr>
            <th>Accessory</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <select id="accessory-dropdown">
                <option hidden value="" disabled selected>Select Accessory</option>
                <?php
                echo wp_nav_menu(
                  array(
                    'theme_location' => 'weight_calculator_accessories',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'walker' => new Select_Nav_Walker()
                  )
                );
                ?>
              </select>
            </td>
            <td>-</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="total-weight-container">
      <table>
        <tr>
          <td>Total Weight</td>
          <td id="total-weight">-</td>
        </tr>
      </table>
    </div>
  </div>
  <?php

  echo ob_get_clean();
});
