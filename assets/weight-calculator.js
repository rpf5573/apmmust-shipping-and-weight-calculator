(() => {
  const addRemoveRowDelegator = (tableId) => {
    const table = document.getElementById(tableId);
    table.addEventListener("click", (e) => {
      if (e.target.classList.contains("delete")) {
        e.target.parentElement.parentElement.parentElement.remove();
        setTimeout(() => {
          calculateAllTotalWeight();
        });
      }
    });
  };

  const changePlaceholder = (select, text) => {
    // Create an option element with the new placeholder text
    const newPlaceholder = document.createElement("option");
    newPlaceholder.textContent = text;
    newPlaceholder.setAttribute("value", "");
    newPlaceholder.setAttribute("disabled", "disabled");
    newPlaceholder.setAttribute("selected", "selected");
    newPlaceholder.setAttribute("hidden", "hidden");

    // Remove the existing placeholder (the first option in the select box)
    select.removeChild(select.firstChild);

    // Add the new placeholder as the first option in the select box
    select.insertBefore(newPlaceholder, select.firstChild);

    select.value = select.options[0].value;
  };

  const convertToKilograms = (grams) => {
    return (grams / 1000).toFixed(2) + "kg";
  };

  window.calculateAllTotalWeight = () => {
    const inputs = [
      ...(document.querySelectorAll(
        '.product-category-weight-table-container input[type="number"]'
      ) ?? []),
    ];
    const $totalWeight = document.querySelector("#total-weight");
    if ($totalWeight === null) return;

    const totalWeight = inputs.reduce((acc, cur) => {
      return acc + cur.value * Number(cur.getAttribute("data-weight"));
    }, 0);
    $totalWeight.innerText = convertToKilograms(totalWeight);
  };

  // 의류 카테고리
  (() => {
    // Get elements from HTML
    const dynamicTable = document.getElementById("clothes-dynamic-table");
    if (dynamicTable === null) return;

    addRemoveRowDelegator("clothes-dynamic-table");
    const categorySelect = document.getElementById("clothes-dropdown");

    // Add event listener to category dropdown
    categorySelect.addEventListener("change", (e) => {
      const weight = e.target.value;
      const category =
        categorySelect.options[categorySelect.selectedIndex].text;
      // Get the index where you want to insert the new row

      // Insert a new row at the specified index
      const newRow = dynamicTable.insertRow(-1);

      // Create a new table cell for the category name
      const categoryCell = newRow.insertCell(0);
      categoryCell.textContent = category.replace(/\s|&nbsp;/g, ""); // Replace with desired category name

      // Create a new table cell for the weight
      const weightCell = newRow.insertCell(1);
      weightCell.innerHTML = `<div class="weight-container d-flex justify-between"><div class="quantity"><input type="number" class="quantity-input" value="1" min="1" data-weight="${weight}" onchange="calculateAllTotalWeight(event)" /></div><button class="delete">X</button></div>`;

      setTimeout(() => {
        changePlaceholder(categorySelect, "Select Clothes");
        calculateAllTotalWeight();
      });
    });
  })(jQuery);

  // 악세서리 카테고리
  (() => {
    // Get elements from HTML
    const dynamicTable = document.getElementById("accessory-dynamic-table");
    if (dynamicTable === null) return;

    addRemoveRowDelegator("accessory-dynamic-table");

    const categorySelect = document.getElementById("accessory-dropdown");

    // Add event listener to category dropdown
    categorySelect.addEventListener("change", (e) => {
      const weight = e.target.value;
      const category =
        categorySelect.options[categorySelect.selectedIndex].text;
      // Get the index where you want to insert the new row

      // Insert a new row at the specified index
      const newRow = dynamicTable.insertRow(-1);

      // Create a new table cell for the category name
      const categoryCell = newRow.insertCell(0);
      categoryCell.textContent = category.replace(/\s|&nbsp;/g, ""); // Replace with desired category name

      // Create a new table cell for the weight
      const weightCell = newRow.insertCell(1);
      weightCell.innerHTML = `<div class="weight-container d-flex justify-between"><div class="quantity"><input type="number" class="quantity-input" value="1" min="1" data-weight="${weight}" onchange="calculateAllTotalWeight(event)" /></div><button class="delete">X</button></div>`;

      setTimeout(() => {
        changePlaceholder(categorySelect, "Select Accessory");
        calculateAllTotalWeight();
      });
    });
  })();
})(jQuery);
