<?php
/**
 * Plugin Name: Apmmust Shipping And Weight Calculator
 * Plugin URI: https://kmong.com/gig/155175
 * Description: apmmust전용 배송비 및 무게 계산기
 * Version: 1.0
 * Author: WPMate
 * Author URI: https://kmong.com/gig/155175
 */

if (!defined('ABSPATH')) {
  exit;
}

require_once plugin_dir_path(__FILE__) . 'shipping-calculator.php';
require_once plugin_dir_path(__FILE__) . 'weight-calculator.php';
